


<div class="col-sm-12">
    <div class="container-fluid">
        <div class="panel panel-success">
            <div class="panel-heading"><center><b>S I G N - U P </b></center></div>
        </div>
    </div>
</div>
<div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">                    
                    <H3>Sign Up:
                    </H3>

                    <hr>
                    
                    <form method="post" action="#">
                        
                        <div class="form-group">
                            <label>Full Name*</label>
                            <input name="txtname" value="{{ old('txtname') }}" placeholder="Full Name" type="text" class="form-control" />
                            <span class="text-danger"></span>
                        </div>
                        
                         
                        <div class="form-group">
                            <label>Mobile Number*</label>
                            <input name="txtcontact1" value="{{ old('txtcontact1') }}" placeholder="10 digit mobile number" maxlength="10" type="text" class="form-control" />
                            <span class="text-danger">{{ $errors->first('txtcontact1') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input name="txtcontact2" {{ old('txtcontact2') }}placeholder="Phone Number" maxlength="10" type="text" class="form-control" />
                        <span class="text-danger">{{ $errors->first('txtcontact2') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Address No.1(Street Name)</label>
                            <input name="txtaddress1" value="{{ old('txtaddress1') }}" placeholder="eg.Hattiban" type="text" class="form-control" />
                        <span class="text-danger">{{ $errors->first('txtaddress1') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Address No.2(Location Name)*</label>
                            <input name="txtaddress2" value="{{ old('txtaddress2') }}" placeholder="eg.Satdobato" type="text" class="form-control" />
                        <span class="text-danger">{{ $errors->first('txtaddress2') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Address No.3(City Name)*</label>
                            <input name="txtaddress3" value="{{ old('txtaddress3') }}" placeholder="eg.Ktm,Lalitpur" type="text" class="form-control" />
                        <span class="text-danger">{{ $errors->first('txtaddress3') }}</span>
                        </div>

                        <div class="form-group">
                            <label>Email*</label>
                            <input name="txtemail" value="{{ old('txtemail') }}" placeholder="eg.email@email.com" type="email" class="form-control" />
                        <span class="text-danger">{{ $errors->first('txtemail') }}</span>
                        </div>
                         
                        <div class="form-group">
                            <label>Password*</label>
                            <input name="txtpwd" placeholder="( 5 to 20 characters)"type="password" class="form-control" />
                         <span class="text-danger">{{ $errors->first('txtpwd') }}</span>
                         </div>

                          <div class="form-group">
                            <label>Confirm Password*</label>
                            <input name="txtcpwd" placeholder="( 5 to 20 characters)"type="password" class="form-control" />
                         <span class="text-danger">{{ $errors->first('txtcpwd') }}</span>
                         </div>
                        <div class="checkbox">
                            <label><input type="checkbox">I agree to terms of use and read all conditions.</label>
                        </div>
                        
                        <div class="form-group">
                            <input name="btnsignup" type="submit" required="" class="btn btn-success" value="Submit" />
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
